let user = {id: ""}

const getUser = () => user;
const setUser = (userData) => Object.assign(user, userData)

export {setUser, getUser};