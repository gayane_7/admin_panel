import firebase from "firebase"
import "firebase/storage"

const config = {
    apiKey: "AIzaSyBo5wFmoWbGM1XqvERjMN7fLJR__z5e0g8",
    authDomain: "project1-e1e17.firebaseapp.com",
    databaseURL: "https://project1-e1e17.firebaseio.com",
    projectId: "project1-e1e17",
    storageBucket: "project1-e1e17.appspot.com",
    messagingSenderId: "753217754036",
    appId: "1:753217754036:web:8fafd5a4bef04617e618e1",
    measurementId: "G-MLXNXZRHCS"
};

export const fire = firebase.initializeApp(config);

export default firebase;

export const firestore = firebase.firestore();
export const auth = firebase.auth();


export const database = firebase.database();