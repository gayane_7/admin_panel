import React from "react";

const SignIn = () =>
{
    return (
        <form>
            <div className="form-group">
                <input type="text" name="username" id="username" className="form-control input-sm"
                       placeholder="Username"/>
            </div>
            <div className="form-group">
                <input type="password" name="password" id="password" className="form-control input-sm"
                       placeholder="Password"/>
            </div>
            <input type="submit" value="SignIn" className="btn btn-info btn-block"/>
        </form>
    );
};

export default SignIn;



