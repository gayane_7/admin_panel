import React, {useState} from "react";
import {fire, firestore} from "../../fire/Fire";


interface IUserData
{
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    confirmPassword: string;
}

const Register = () =>
{
   const [data, setData] = useState<IUserData>({
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        password: "",
        confirmPassword: ""
    });

    function handleChange(e: any)
    {
        let {value, name} = e.target;
        setData({
            ...data,
            ...{[name]:  value}
        });
    }

    const onSubmit = (event: any) =>
    {
        event.preventDefault();
        fire.auth().createUserWithEmailAndPassword(data.email, data.password).then((user) =>
        {

            firestore.collection("users").doc((user as any).user.uid).set({
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                userName: data.username,
                id: user.user?.uid,
            });
        });
    };

    return (
        <form onSubmit={onSubmit}>
            <div className="row">
                <div className="col-xs-6 col-sm-6 col-md-6">
                    <div className="form-group">
                        <input type="text" name="firstName" id="firstName" value={data.firstName}
                               onChange={handleChange}
                               required
                               className="form-control input-sm"
                               placeholder="First Name"/>
                        {/*{errors.firstName && <p className="error-msg">This field is required</p>}*/}
                    </div>
                </div>
                <div className="col-xs-6 col-sm-6 col-md-6">
                    <div className="form-group">
                        <input type="text" name="lastName" id="lastName" value={data.lastName}
                               onChange={handleChange}
                               required
                               className="form-control input-sm"
                               placeholder="Last Name"/>
                        {/*{errors.lastNname && <p className="error-msg">This field is required</p>}*/}
                    </div>
                </div>
            </div>
            <div className="form-group">
                <input type="text" name="username" id="username" value={data.username}
                       onChange={handleChange}
                       required
                       className="form-control input-sm"
                       placeholder="Username"/>
                {/*{errors.username && <p className="error-msg">{errors.username.message}</p>}*/}
            </div>
            <div className="form-group">
                <input type="email" name="email" id="email" value={data.email}
                       onChange={handleChange}
                       required
                       className="form-control input-sm"
                       placeholder="Email Address"/>
                {/*{errors.email && <p className="error-msg">This field is required</p>}*/}
            </div>

            <div className="row">
                <div className="col-xs-6 col-sm-6 col-md-6">
                    <div className="form-group">
                        <input type="password" name="password" id="password"
                               value={data.password}
                               onChange={handleChange}
                               required
                               className="form-control input-sm"
                               placeholder="Password"/>
                        {/*{errors.password && <p className="error-msg">{errors.password.message}</p>}*/}
                    </div>
                </div>
                <div className="col-xs-6 col-sm-6 col-md-6">
                    <div className="form-group">
                        <input type="password" name="confirmPassword" value={data.confirmPassword} id="confirmPassword"
                               onChange={handleChange}
                               required
                               className="form-control input-sm" placeholder="Confirm Password"/>

                        {/*{errors.confirm_password && <p className="error-msg">{errors.confirm_password.message}</p>}*/}
                    </div>
                </div>
            </div>

            <input type="submit" className="btn btn-info btn-block"/>
        </form>
    );
};

export default Register;



