import React, {lazy, Suspense} from "react";
import {BrowserRouter as Router, NavLink, Redirect, Route, Switch} from "react-router-dom";
import {getUser} from "../../model/UserModel";

const Profile = lazy(() => import("../content/Profile"));
const Films = lazy(() => import("../content/Films"));


const SideBar = () =>
{
    const id = getUser()["id"];
    const SideBarData = [
        {
            name: "Profile",
            path: "/" + id,
            component: Profile
        },
        {
            name: "Films",
            path: "/films",
            component: Films
        }
    ];


    return (
        <Router>
            <nav className="col-md-2 d-none d-md-block bg-light sidebar pt-5">
                <div className="list-group list-group-flush">
                    {SideBarData.map((item: any, index: number) =>
                        <NavLink key={index} to={item.path} className="list-group-item list-group-item-action bg-light"
                                 activeClassName="item-active">{item.name}</NavLink>
                    )}
                </div>
            </nav>
            <Suspense fallback={<div className="loader"/>}>
                <Switch>
                    <Redirect exact from="/" to={"/" + id}/>
                    {SideBarData.map((item: any, index: number) => <Route path={item.path} key={index}
                                                                          component={item.component}/>)}
                </Switch>
            </Suspense>
        </Router>
    );
};

export default SideBar;