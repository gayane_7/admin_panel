import React from "react";
import SideBar from "./ side_bar/SideBar";

const Home = () =>
{
   return (
        <section className="row h-100">
            <SideBar/>
        </section>
    );
};

export default Home;