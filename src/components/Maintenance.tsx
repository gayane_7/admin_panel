import React from "react";
import Modal from "./Modal";
import Register from "./sign_in/Registration";
import SignIn from "./sign_in/SignIn";

const Maintenance = () =>
{

    const RegisterHeader = "Register";
    const SignInHeader = "Sign In";
    const registerButton = <span>Register</span>;
    const signInButton = <span>Sign In</span>;
    return (
        <section className="d-flex align-items-center justify-content-center pt-5">
            <Modal header={SignInHeader} button={signInButton} btnClass="btn-outline-primary mr-2">
                <SignIn/>
            </Modal>
            <Modal header={RegisterHeader} button={registerButton} btnClass="btn-outline-secondary">
                <Register/>
            </Modal>
        </section>
    );
};

export default Maintenance;