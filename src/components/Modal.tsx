import React, {useState} from "react";

const Modal: React.FC<{ header?: JSX.Element | string, children: JSX.Element, button?: JSX.Element, btnClass?: string }> = ({header, children, button, btnClass}) =>
{
    const [showModal, setShowModal] = useState(false);

    return (
        <>
            {
                button ? (
                    <button onClick={() => changeModal(true)} className={`btn btn-sm ${btnClass}`}>
                        {button}
                    </button>
                ) : null
            }
            {
                showModal ? (
                    <div className="modal-backdrop" onClick={() => changeModal(false)}>
                        <div className="modal_content" onClick={e => e.stopPropagation()}>
                            <div className="modal_header">{header}</div>
                            {children}
                        </div>
                    </div>
                ) : null
            }

        </>
    );

    function changeModal(showModal: boolean)
    {
        setShowModal(showModal);
    }
};


export default Modal;