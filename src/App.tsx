import React, {useState} from "react";
import Loader from "./components/Loader";
import {fire, firestore} from "./fire/Fire";
import Home from "./components/Home";
import Maintenance from "./components/Maintenance";
import {setUser} from "./model/UserModel";

function App()
{
    let [authorized, setAuthorized] = useState<boolean | null>(null);

    React.useEffect(() =>
    {
        let unsubscribe = fire.auth().onAuthStateChanged((user: any) =>
        {
            if (user)
            {
                firestore.collection("users").doc(user.uid).get().then((doc: any) =>
                {
                    setUser(doc.data());
                    setAuthorized(true);
                });
            } else
            {
                setAuthorized(false);
            }
        });

        return () =>
        {
            unsubscribe();
        };
    }, []);
    return (
        <div className="container-fluid main_page">
            {
                authorized === null ? <Loader/> :
                    authorized === true ?
                        <Home/> : (
                            <Maintenance/>
                        )
            }
        </div>
    );
}

export default App;